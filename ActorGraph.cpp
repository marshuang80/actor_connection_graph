/*
 * ActorGraph.cpp
 * Author: Adrian Guthals
 * Date: 2/24/2015
 *
 * This file is meant to exist as a container for starter code that you can use to read the input file format
 * defined in movie_casts.tsv. Feel free to modify any/all aspects as you wish.
 */
#include "actorNode.hpp"
#include "ActorGraph.hpp" 
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>

using namespace std;
using std::make_pair;
ActorGraph::ActorGraph(void) {}


unordered_set<actorNode*, Hash1, Equal1> ActorGraph::loadFromFile(const char* in_filename, bool use_weighted_edges)
{
  
  // Initialize the file stream
  ifstream infile(in_filename);
  unordered_map<pair<string, int>, vector<actorNode*>, Hash, Equal> movies;
  unordered_set<actorNode*, Hash1, Equal1> actors; 

  bool have_header = false;
  
  // keep reading lines until the end of file is reached
  while (infile)
  {
    string s;
    
    // get the next line
    if (!getline( infile, s )) break;
    
    if (!have_header)
    {
      // skip the header
      have_header = true;
      continue;
    }

    istringstream ss( s );
    vector <string> record;

    while (ss)
    {
      string next;
      
      // get the next string before hitting a tab character and put it in 'next'
      if (!getline( ss, next, '\t' )) break;
      
      record.push_back( next );
    }
    
    if (record.size() != 3)
    {
      // we should have exactly 3 columns
      continue;
    }
    
    string actor_name(record[0]);
    string movie_title(record[1]);
    int movie_year = stoi(record[2]);
    pair<string, int> movie (movie_title, movie_year); 
    actorNode* actor_node = new actorNode(actor_name);
    if(actors.find(actor_node) != actors.end()) actor_node = *(actors.find(actor_node));
    else  actors.insert(actor_node); 

    if(movies.find(movie)!=movies.end()) movies[movie].push_back(actor_node);
    else{
        vector<actorNode*> movie_stars;
        movie_stars.push_back(actor_node);
        pair<pair<string,int>,vector<actorNode*>> movie_pair(movie, movie_stars);  
        movies.insert(movie_pair);
    }
    // we have an actor/movie relationship, now what?
  }

pair<string, int> test = make_pair("ELEPHANT WHITE",2011);
auto moviesItr = movies.begin();
for (; moviesItr != movies.end(); ++moviesItr) {
    for (auto& i : moviesItr->second) {
        auto itr = actors.find(i);
        for(auto& j:moviesItr->second){
            if (j->actorName == i->actorName) continue; 
//            if (moviesItr == movies.find(test) && i->actorName == "BACON, KEVIN (I)") cout<< j->actorName<<endl;
            pair<actorNode*, pair<string, int>> temp = make_pair(j, moviesItr->first);
            (*itr)->aList.insert(temp);
        }
    }
}
//pair<string, int> test = make_pair("ELEPHANT WHITE",2011);
//vector<actorNode*> test2 = (movies.at(test)); 
//for(auto i: test2) cout<< i->actorName<<endl; 
//actorNode* bacon = new actorNode("BACON, KEVIN (I)");
//actorNode* temp1 = *(actors.find(bacon));
//for(auto i:temp1->aList) cout<<i.first->actorName << " "<< i.second.first<< " " << i.second.second<<endl; 
// testing
  if (!infile.eof())
  {
    cerr << "Failed to read " << in_filename << "!\n";
  }
  infile.close();
  return actors;
}
