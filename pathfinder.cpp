#include "actorNode.hpp"
#include "ActorGraph.hpp"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <queue>
#include <unordered_set>
#include <algorithm>
#include <limits>

//argv[1]: file containing movie_casts (tsv)
//argv[2]: u or w-> weighted or unweighted paths
//argv[3]: actor pairs tsv file
//argv[4]: outfile name 

using namespace std;
using std::make_pair;

void resetFlags(actorNode* source, actorNode* end, unordered_set<actorNode*, Hash1, Equal1> actors) {
    actorNode* start = *(actors.find(source));
    queue<actorNode*> bfs;
    bfs.push(start);
    while (!bfs.empty()) {
        actorNode* curr = bfs.front();
        bfs.pop();
        for (auto& i: curr->aList) {
            if(i.first->distance != numeric_limits<int>::max()) {// && i.first->actorName != start->actorName){
                int max = numeric_limits<int>::max();
                i.first->distance = max;
                i.first->prev = nullptr;
                //if(i.first->actorName == end->actorName) return;
                bfs.push(i.first);
            }  
        }
    }
}

class weightComp{
    public:
        bool operator()(actorNode*& first, actorNode*&second)const{
            return first < second; 
        }
};

int main (int argc, char* argv[]) {
    if (argc < 5) return -1;
    bool have_header = false;
    ActorGraph graph;
      unordered_set<actorNode*, Hash1, Equal1> actors = graph.loadFromFile(argv[1], string(argv[2])=="u"?false:true);
    
    ofstream out;
    out.open(argv[4]);
    out<<"(actor)--[movie#@year]-->(actor)--..."<<endl;
    ifstream in(argv[3]);
    while (in) {
        string s;
        if (!getline(in, s)) break;
        if (!have_header) {
            have_header = true;
            continue;
        }
        istringstream ss(s);
        vector<string> actorPair;
        while (ss) {
            string next;
            if (!getline(ss, next, '\t')) break;
            actorPair.push_back(next);
        }
        if (actorPair.size() != 2) {
            continue;
        }
        actorNode* actor1 = new actorNode(actorPair[0]);
        actorNode* actor2 = new actorNode(actorPair[1]);
        if(string(argv[2]) == "u" ) {
            actorNode* start = *(actors.find(actor1));
            start->distance = 0; 
            queue<actorNode*> bfs;
            bfs.push(start);
            bool y = false; 
            while(!bfs.empty()){
                actorNode* curr = bfs.front(); 
                bfs.pop(); 
                for(auto& i : curr->aList) {
                      if(i.first->distance == numeric_limits<int>::max()){
                        i.first->distance  = (curr->distance)+1;
                        i.first->prev = curr;
                        i.first->prev_movie = i.second;
                        if(i.first->actorName == actor2->actorName){
                            actor2=i.first; 
                            y=true; 
                            break; 
                        }
                        bfs.push(i.first);
                    }  
                }
               if(y) break; 
            }
        }
        else {   
            actorNode* start = *(actors.find(actor1));
            start->distance = 0; 
            priority_queue<actorNode*,vector<actorNode*>,weightComp> pq;
            pq.push(start);
            bool y = false; 
            while(!pq.empty()){
                actorNode* curr = pq.top(); 
                pq.pop(); 
                for(auto& i : curr->aList) {
                    int distance = curr->distance + (2016-i.second.second); 
                    if(distance < i.first->distance){
                        i.first->distance  = distance;
                        i.first->prev = curr;
                        i.first->prev_movie = i.second;
                        if(i.first->actorName == actor2->actorName && i.first->distance < actor2->distance){
                            actor2=i.first; 
                            y=true; 
                        }
                        pq.push(i.first);
                    }
//                    if(y) break;   
                }
//                if(y) break; 
            }
             
        }
        vector<string> toPrint;
        actorNode* temp = actor2; 
        while(temp->prev != nullptr){
            string moviename = string(temp->prev_movie.first); 
            string movieyear = to_string(temp->prev_movie.second); 
            string Movie ="--[" + moviename + "#@" + movieyear + "]";
            string Actor = "-->(" + string(temp->actorName) + ")";
            toPrint.push_back(Actor);
            toPrint.push_back(Movie);
            temp = temp->prev; 
        }
        string mainActor = "(" + actor1->actorName + ")";
        toPrint.push_back(mainActor); 
        reverse(toPrint.begin(), toPrint.end());
        for (auto elem : toPrint) out<<elem;
        out<<endl;         
        resetFlags(*(actors.find(actor1)), *(actors.find(actor2)), actors);
        
    }
    out.close(); 
    in.close(); 
    return 0;
}
