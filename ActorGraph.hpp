/*
 * ActorGraph.hpp
 * Author: Adrian Guthals
 * Date: 2/24/2015
 *
 * This file is meant to exist as a container for starter code that you can use to read the input file format
 * defined in movie_casts.tsv. Feel free to modify any/all aspects as you wish.
 */

#ifndef ACTORGRAPH_HPP
#define ACTORGRAPH_HPP

#include <iostream>
#include <unordered_set>

// Maybe include some data structures here

using namespace std;


class Hash{
    public: 
        size_t operator()(pair<string, int> const& key) const{
            size_t hash_num = 0; 
            for(size_t i=0; i<key.first.size();i++) hash_num+= 31*hash_num+key.first[i]+(2016-key.second);
            return hash_num;
        }
    };

class Equal{
    public:
        bool operator() (pair<string, int> const& key1, pair<string, int> const& key2) const {
            return (key1.first == key2.first && key1.second == key2.second); 
        }
};

class Hash1{
    public: 
        size_t operator()(actorNode* const& key) const{
            size_t hash_num = 0; 
            for(size_t i = 0; i <key->actorName.size(); i++) hash_num += 31*hash_num+key->actorName[i];
            return hash_num;
        }
};

class Equal1{
    public: 
        bool operator() (actorNode* const& key1, actorNode* const& key2) const {
            return (key1->actorName == key2->actorName); 
        }
};



class ActorGraph {
protected:
  
  // Maybe add class data structure(s) here

public:
  ActorGraph(void);

  // Maybe add some more methods here
  
  /** You can modify this method definition as you wish
   *
   * Load the graph from a tab-delimited file of actor->movie relationships.
   *
   * in_filename - input filename
   * use_weighted_edges - if true, compute edge weights as 1 + (2015 - movie_year), otherwise all edge weights will be 1
   *
   * return true if file was loaded sucessfully, false otherwise
   */
  unordered_set<actorNode*,Hash1, Equal1> loadFromFile(const char* in_filename, bool use_weighted_edges);
  
};
#endif // ACTORGRAPH_HPP
