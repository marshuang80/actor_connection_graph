#ifndef ACTORNODE_HPP
#define ACTORNODE_HPP
#include <string>
#include <map>
#include <limits>
using namespace std; 

class actorNode{
    public:
        string actorName;
        actorNode* prev = nullptr;
        int distance = numeric_limits<int>::max();
        pair<string,int> prev_movie;
        multimap<actorNode*,std::pair<string, int>> aList;
        actorNode(string inActor):actorName(inActor){};
        bool visited = false;
        bool operator > (const actorNode& other){
            if(this->distance > other.distance) return true; 
            else return false; 
        }
    private: 
};

#endif // ACTORNODE_HPP
